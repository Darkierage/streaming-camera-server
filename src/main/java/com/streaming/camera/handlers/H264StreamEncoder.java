package com.streaming.camera.handlers;

import com.streaming.camera.handlers.frame.FrameEncoder;
import com.streaming.camera.utils.ImageUtils;
import com.xuggle.xuggler.*;
import com.xuggle.xuggler.IPixelFormat.Type;
import com.xuggle.xuggler.IStreamCoder.Direction;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.video.IConverter;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class H264StreamEncoder extends OneToOneEncoder {
    private final static Logger logger = LoggerFactory.getLogger(Logger.class);
    private final IStreamCoder iStreamCoder = IStreamCoder.make(Direction.ENCODING, ICodec.ID.CODEC_ID_H264);
    private final IPacket iPacket = IPacket.make();
    private long startTime;
    private final Dimension dimension;
    private final FrameEncoder frameEncoder;

    public H264StreamEncoder(Dimension dimension, boolean usingInternalFrameEncoder) {
        super();
        this.dimension = dimension;
        if (usingInternalFrameEncoder) {
            frameEncoder = new FrameEncoder(4);
        } else {
            frameEncoder = null;
        }
        initialize();
    }

    private void initialize() {
        iStreamCoder.setNumPicturesInGroupOfPictures(25);

        iStreamCoder.setBitRate(200000);
        iStreamCoder.setBitRateTolerance(10000);
        iStreamCoder.setPixelType(Type.YUV420P);
        iStreamCoder.setHeight(dimension.height);
        iStreamCoder.setWidth(dimension.width);
        iStreamCoder.setFlag(IStreamCoder.Flags.FLAG_QSCALE, true);
        iStreamCoder.setGlobalQuality(0);

        IRational rate = IRational.make(25, 1);
        iStreamCoder.setFrameRate(rate);
        iStreamCoder.setTimeBase(IRational.make(rate.getDenominator(), rate.getNumerator()));

        IMetaData codecOptions = IMetaData.make();
        codecOptions.setValue("tune", "zerolatency");

        int revl = iStreamCoder.open(codecOptions, null);
        if (revl < 0) {
            throw new RuntimeException("could not open the coder");
        }
    }

    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel,
                            Object msg) throws Exception {
        return encode(msg);
    }

    public Object encode(Object msg) throws Exception {
        if (msg == null) {
            return null;
        }
        if (!(msg instanceof BufferedImage)) {
            throw new IllegalArgumentException();
        }

        logger.info("encode the frame");
        BufferedImage bufferedImage = (BufferedImage) msg;
        BufferedImage convetedImage = ImageUtils.convertToType(bufferedImage, BufferedImage.TYPE_3BYTE_BGR);
        IConverter converter = ConverterFactory.createConverter(convetedImage, Type.YUV420P);
        long now = System.currentTimeMillis();
        if (startTime == 0) {
            startTime = now;
        }
        IVideoPicture pFrame = converter.toPicture(convetedImage, (now - startTime) * 1000);
        iStreamCoder.encodeVideo(iPacket, pFrame, 0);
        pFrame.delete();
        converter.delete();

        if (iPacket.isComplete()) {
            try {
                ByteBuffer byteBuffer = iPacket.getByteBuffer();
                if (iPacket.isKeyPacket()) {
                    logger.info("key frame");
                }
                ChannelBuffer channelBuffe = ChannelBuffers.copiedBuffer(byteBuffer.order(ByteOrder.BIG_ENDIAN));
                if (frameEncoder != null) {
                    return frameEncoder.encode(channelBuffe);
                }

                return channelBuffe;
            } finally {
                iPacket.reset();
            }
        } else {
            return null;
        }
    }
}
