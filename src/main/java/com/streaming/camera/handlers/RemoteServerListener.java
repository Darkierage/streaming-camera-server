package com.streaming.camera.handlers;

public interface RemoteServerListener {
    void onRemoteCommandReceived(byte command);
}
