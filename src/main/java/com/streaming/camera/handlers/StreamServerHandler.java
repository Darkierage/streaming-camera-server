package com.streaming.camera.handlers;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StreamServerHandler extends SimpleChannelHandler {
    private final static Logger logger = LoggerFactory.getLogger(StreamServerHandler.class);

    private final StreamServerListener streamServerListener;
    private final RemoteServerListener remoteServerListener;

    public StreamServerHandler(StreamServerListener streamServerListener, RemoteServerListener remoteServerListener) {
        super();

        this.streamServerListener = streamServerListener;
        this.remoteServerListener = remoteServerListener;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        Channel channel = e.getChannel();
        Throwable t = e.getCause();
        logger.error("exception caught at :{},exception :{}", channel, t);
        streamServerListener.onException(channel, t);
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        Channel channel = e.getChannel();
        logger.info("channel connected :{}", channel);
        streamServerListener.onClientConnectedIn(channel);
        super.channelConnected(ctx, e);
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx,
                                    ChannelStateEvent e) throws Exception {
        Channel channel = e.getChannel();
        logger.info("channel disconnected :{}", channel);
        streamServerListener.onClientDisconnected(channel);
        super.channelDisconnected(ctx, e);
    }

    @Override
    public void writeComplete(ChannelHandlerContext ctx, WriteCompletionEvent e)
            throws Exception {
        Channel channel = e.getChannel();
        long size = e.getWrittenAmount();
        logger.info("frame send at :{} size :{}", channel, size);
        super.writeComplete(ctx, e);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
            throws Exception {
        ChannelBuffer channelBuffer = (ChannelBuffer) e.getMessage();
        remoteServerListener.onRemoteCommandReceived(channelBuffer.getByte(0));
//        logger.info("!!!--message received :{}", test);
        super.messageReceived(ctx, e);
    }
}