package com.streaming.camera.channel;

import com.streaming.camera.handlers.RemoteServerListener;
import com.streaming.camera.handlers.StreamServerHandler;
import com.streaming.camera.handlers.StreamServerListener;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.codec.frame.LengthFieldPrepender;

public class StreamServerChannelPipelineFactory implements ChannelPipelineFactory {
    private final StreamServerListener streamServerListener;
    private final RemoteServerListener remoteServerListener;

    public StreamServerChannelPipelineFactory(
            StreamServerListener streamServerListener, RemoteServerListener remoteServerListener) {
        super();

        this.streamServerListener = streamServerListener;
        this.remoteServerListener = remoteServerListener;
    }

    @Override
    public ChannelPipeline getPipeline() {
        ChannelPipeline pipeline = Channels.pipeline();
        pipeline.addLast("frame encoder", new LengthFieldPrepender(4, false));
        pipeline.addLast("stream server handler", new StreamServerHandler(streamServerListener, remoteServerListener));

        return pipeline;
    }
}