package com.streaming.camera;

import au.edu.jcu.v4l4j.*;
import au.edu.jcu.v4l4j.exceptions.V4L4JException;
import com.streaming.camera.utils.NativeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class StreamingCamera implements CaptureCallback {
    static {
        try {
            NativeUtils.loadLibraryFromJar("/META-INF/native", new String[]{"video", "v4l4j"});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(StreamingCamera.class);

    private Imager imager;

    private VideoDevice device;
    private JPEGFrameGrabber grabber;

    public StreamingCamera() throws Exception {
        device = new VideoDevice("/dev/video0");

        ImageFormatList ifl = device.getDeviceInfo().getFormatList();
        List<ImageFormat> formats = ifl.getNativeFormats();

        imager = new Imager();

        //RawFrameGrabber grabber = device.getRawFrameGrabber(640, 480, 0, 0, formats.get(0));
        grabber = device.getJPEGFrameGrabber(320, 240, 0, 0, 100, formats.get(0));
        grabber.setCaptureCallback(this);
        grabber.startCapture();

        logger.info("capture started");
        logger.info("format: " + formats.get(0).getName());

        //latch.await(10, TimeUnit.SECONDS);
        //grabber.stopCapture();
        //device.releaseFrameGrabber();
        //device.releaseControlList();
        //device.release();
    }

    public List<ImageFormat> GetNativeFormatsList(ImageFormatList ifl) {
        List<ImageFormat> formats = ifl.getNativeFormats();

        System.out.println("Native formats: ");
        for (ImageFormat v : formats) {
            //System.out.pritnln(v.getName());
            //logger.error("{}", v.getName());
            System.out.println(v.getName());
        }

        formats = ifl.getJPEGEncodableFormats();

        System.out.println("JPEG formats: ");
        for (ImageFormat v : formats) {
            System.out.println(v.getName());
        }

        formats = ifl.getRGBEncodableFormats();

        System.out.println("RGB formats: ");
        for (ImageFormat v : formats) {
            System.out.println(v.getName());
        }

        formats = ifl.getBGREncodableFormats();

        System.out.println("BGR formats: ");
        for (ImageFormat v : formats) {
            System.out.println(v.getName());
        }

        formats = ifl.getYUVEncodableFormats();

        System.out.println("YUV formats: ");
        for (ImageFormat v : formats) {
            System.out.println(v.getName());
        }

        formats = ifl.getYVUEncodableFormats();

        System.out.println("YVU formats: ");
        for (ImageFormat v : formats) {
            System.out.println(v.getName());
        }

        formats = ifl.getNativeFormats();

        return formats;
    }

    Imager getImager() {
        return imager;
    }

    public void stop() {
        grabber.stopCapture();
        device.release();
    }

    @Override
    public void nextFrame(VideoFrame frame) {
        try {
            //ImageIO.write(frame.getBufferedImage(), "JPG", new File("debugImages/jpegImage-" + (i++) + ".jpg"));
            imager.setImage(frame.getBufferedImage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            frame.recycle();
        }
    }

    @Override
    public void exceptionReceived(V4L4JException e) {
        e.printStackTrace();
    }
}