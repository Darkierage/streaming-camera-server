package com.streaming.camera;

import java.awt.image.BufferedImage;

class Imager {
	private BufferedImage image;

	void setImage(BufferedImage image) {
		this.image = image;
	}

	BufferedImage getImage() {
		return this.image;
	}
}