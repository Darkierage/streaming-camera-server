package com.streaming.camera.gpio;

import java.util.Random;

public class Controller {
    private final int PinNumber = 1;
    private final int PinNumber2 = 4;

    private final ServoMotor servo;
    private final ServoMotor servo2;

    public Controller() {
        servo = new ServoMotor(PinNumber);
        servo2 = new ServoMotor(PinNumber2);
    }

    //45 degree angle
    public void moveRight() {
        int position = servo.getPosition();
        position = position + 5;
        servo.setPosition(position);

        int position2 = servo2.getPosition();
        position2 = position2 + 5;
        servo2.setPosition(position2);
    }

    //45 degree angle
    public void moveLeft() {
        int position = servo.getPosition();
        position = position - 5;
        servo.setPosition(position);

        int position2 = servo2.getPosition();
        position2 = position2 - 5;
        servo2.setPosition(position2);
    }

    public int generateRandomNumber(int max) {
        Random random = new Random();
        return random.nextInt(max + 1);
    }

    public void setOnMiddleAll() {
        servo.setPosition(10);
        servo2.setPosition(10);
    }


}
