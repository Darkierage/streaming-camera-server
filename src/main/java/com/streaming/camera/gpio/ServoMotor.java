package com.streaming.camera.gpio;

import com.pi4j.wiringpi.*;

public class ServoMotor
{
    //The motor position
    private int value;
    //The Pin number
    private int PinNumber;

    public ServoMotor(int pinNumber)
    {
        PinNumber = pinNumber;
        value = 15;

        Gpio.wiringPiSetup();
        SoftPwm.softPwmCreate(PinNumber,0,100);
        sleepMillisec(1500);

        SoftPwm.softPwmWrite(PinNumber, value);
        //allow sufficient time to the servo mottor to move to the position.
        sleepMillisec(1500);
        //stop sending orders to the motor.
        stop();
    }

    public void setPosition(int positionNumber)
    {
        if (positionNumber >= 0 && positionNumber <= 25)
        {
//            update the position;
            value = positionNumber;
            //send the value to the motor.
            SoftPwm.softPwmWrite(PinNumber, value);
            //give time to the motor to reach the position
            sleepMillisec(1500);
            //stop sending orders to the motor.
            stop();
        }
        
    }
    
   
    private void stop(){
        //zero tells the motor to turn itself off and wait for more instructions.
        SoftPwm.softPwmWrite(PinNumber, 0);
    }
    
   
    public int getPosition(){
        //returns the current position;
        return value;
    }
    
 
    private void sleepMillisec(int millisec){
        try
        {
            Thread.sleep(millisec);
        }
        catch ( InterruptedException e)
        {
        }
    }
}
