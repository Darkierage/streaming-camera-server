package com.streaming.camera;

import java.net.InetSocketAddress;

public class App {
    public static void main(String[] args) throws Exception {
        new Server(new StreamingCamera()).start(new InetSocketAddress("192.168.0.241", 20000));
    }
}