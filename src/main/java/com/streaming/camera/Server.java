package com.streaming.camera;

import com.streaming.camera.channel.StreamServerChannelPipelineFactory;
import com.streaming.camera.gpio.Controller;
import com.streaming.camera.handlers.H264StreamEncoder;
import com.streaming.camera.handlers.RemoteServerListener;
import com.streaming.camera.handlers.StreamServerListener;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.SocketAddress;
import java.util.concurrent.*;

public class Server {

    private final static Logger logger = LoggerFactory.getLogger(Server.class);

    private Controller gpioController;

    private Imager imager;
    private StreamingCamera streamingCamera;
    private volatile boolean isStreaming;
    private final Dimension dimension;

    private final ChannelGroup channelGroup = new DefaultChannelGroup();
    private final ServerBootstrap serverBootstrap;
    private final H264StreamEncoder h264StreamEncoder;

    private ScheduledExecutorService timeWorker;
    private ExecutorService encodeWorker;
    private int FPS = 1;
    private ScheduledFuture<?> imageGrabTaskFuture;

    Server(StreamingCamera streamingCamera) {
        this.streamingCamera = streamingCamera;
        this.imager = streamingCamera.getImager();
        this.dimension = new Dimension(320, 240);

        gpioController = new Controller();

        this.serverBootstrap = new ServerBootstrap();
        this.serverBootstrap.setFactory(new NioServerSocketChannelFactory(
                Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool()));
        this.serverBootstrap.setPipelineFactory(new StreamServerChannelPipelineFactory(
                new StreamServerListenerIMPL(),
                new StreamFrameListenerIMPL()));
        this.timeWorker = new ScheduledThreadPoolExecutor(1);
        this.encodeWorker = Executors.newSingleThreadExecutor();
        this.h264StreamEncoder = new H264StreamEncoder(dimension, false);
    }

    public void start(SocketAddress streamAddress) {
        logger.info("Server started :{}", streamAddress);
        Channel channel = serverBootstrap.bind(streamAddress);
        channelGroup.add(channel);
    }

    public void stop() {
        logger.info("server is stoping");
        channelGroup.close();
        timeWorker.shutdown();
        encodeWorker.shutdown();
        serverBootstrap.releaseExternalResources();
        streamingCamera.stop();
    }

    public void setImager(Imager imager) {
        this.imager = imager;
    }

    private class StreamServerListenerIMPL implements StreamServerListener {

        @Override
        public void onClientConnectedIn(Channel channel) {
            channelGroup.add(channel);
            if (!isStreaming) {
                Runnable imageGrabTask = new ImageGrabTask();
                imageGrabTaskFuture = timeWorker.scheduleWithFixedDelay(imageGrabTask,
                        0,
                        1000 / FPS,
                        TimeUnit.MILLISECONDS);
                isStreaming = true;
            }
        }

        @Override
        public void onClientDisconnected(Channel channel) {
            channelGroup.remove(channel);
            int size = channelGroup.size();
            logger.info("current connected clients :{}", size);
            if (size == 1) {
                imageGrabTaskFuture.cancel(false);
                isStreaming = false;
                streamingCamera.stop();
            }
        }

        @Override
        public void onException(Channel channel, Throwable t) {
            channelGroup.remove(channel);
            channel.close();
            int size = channelGroup.size();
            logger.info("current connected clients :{}", size);
            if (size == 1) {
                imageGrabTaskFuture.cancel(false);
                isStreaming = false;

            }
        }

        volatile long frameCount = 0;

        private class ImageGrabTask implements Runnable {

            @Override
            public void run() {
                logger.info("image grabed ,count :{}", frameCount++);
                BufferedImage bufferedImage = imager.getImage();
                encodeWorker.execute(new EncodeTask(bufferedImage));
            }
        }

        private class EncodeTask implements Runnable {
            private final BufferedImage image;

            EncodeTask(BufferedImage image) {
                super();
                this.image = image;
            }

            @Override
            public void run() {
                try {
                    Object msg = h264StreamEncoder.encode(image);
                    if (msg != null) {
                        channelGroup.write(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class StreamFrameListenerIMPL implements RemoteServerListener {
        @Override
        public void onRemoteCommandReceived(byte command) {
            switch (command) {
                case 1:
                    gpioController.moveLeft();
                    break;
                case 2:
                    gpioController.moveRight();
                    break;
            }
        }
    }
}